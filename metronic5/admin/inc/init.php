<?php
// Config
global $_CONFIG;

// Main configs
include_once __DIR__ . "/config/main.php";

// Libs
include_once __DIR__ . "/lib/Util.php";
include_once __DIR__ . "/lib/Layout.php";
include_once __DIR__ . "/lib/Menu.php";
include_once __DIR__ . "/lib/Page.php";

// "Default" demo configs
include_once __DIR__ . "/config/demos/default/demo.php";
include_once __DIR__ . "/config/demos/default/layout.php";
include_once __DIR__ . "/config/demos/default/pages.php";
include_once __DIR__ . "/config/demos/default/menu.php";

// "Demo2" demo configs
include_once __DIR__ . "/config/demos/demo2/demo.php";
include_once __DIR__ . "/config/demos/demo2/layout.php";
include_once __DIR__ . "/config/demos/demo2/pages.php";
include_once __DIR__ . "/config/demos/demo2/menu.php";