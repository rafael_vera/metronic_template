<?php
header("Content-Type: application/json");
$files = $_POST['files'];
$retval = array("js"=>array(), "css"=>array());
array_push($retval['js'], get($files['js']));
array_push($retval['css'], get($files['css']));

echo json_encode($retval);

function getTime($file)
{
    if (file_exists($file)) {    
        return array("url"=>$file, "time"=>filemtime($file));
    } else {
        return array("url"=>$file);
    }
}

function get($bundle)
{
    $ret = array();
    if (isset($bundle['url'])) {
        $ret['url'] = getTime($bundle['url']);
    }
    if (isset($bundle['childs'])) {
        $ret['childs']=array();
        foreach ($bundle['childs'] as $value) {
            array_push($ret['childs'], get($value));
        }
    }
    if (isset($bundle['childsNodeps'])) {
        $ret['childsNodeps']=array();
        foreach ($bundle['childsNodeps'] as $value) {
            array_push($ret['childsNodeps'], getTime($value));
        }
    }
    return $ret;
}